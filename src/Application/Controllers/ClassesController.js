import SuccessResponse from "@responses/SuccessResponse";
import NotFoundResponse from "@responses/NotFoundResponse";
import { ClassRepository } from "@repositories/ClassRepository";
import InternalServerErrorResponse from "@responses/InternalServerErrorResponse";
import { CreateClassService } from "@domain/Classes/Services/CreateClassService";
import { DeleteClassService } from "@domain/Classes/Services/DeleteClassService";
import { UpdateClassService } from "@domain/Classes/Services/UpdateClassService";

const classes = {};

classes.index = async (req, res) => {
	try {
		let data = [];
		if (req.headers.role === "ADMIN") data = await new ClassRepository().list(req.query.page, req.query.per_page).admin().then(result => { return result.get() });
		return SuccessResponse(res, "List classes", data);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, exception.message);
	}
};

classes.show = async (req, res) => {
	try {
		const data = await new ClassRepository().detailById(req.params.class_id).then(result => { return result.get() });
		console.log(data);
		return SuccessResponse(res, "Detail class.", data);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof ClassNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, exception.message);
		}
	}
};

classes.store = async (req, res) => {
	try {
		const result = await new CreateClassService(req.body).persist();
		return SuccessResponse(res, "Successfully create new class!", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, exception.message);
	}
};

classes.update = async (req, res) => {
	try {
		const result = new UpdateClassService(req.params.class_id, req.body).persist();
		return SuccessResponse(res, "Successfully update class data!", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof ClassNotFoundException) {
			return NotFoundResponse("The class you're looking for couldn't be found!");
		} else {
			return InternalServerErrorResponse(exception.message);
		}
	}
};

classes.destroy = async (req, res) => {
	try {
		const result = await new DeleteClassService(req.params.class_id).persist();
		return SuccessResponse(res, "Successfully delete class!", null);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof ClassNotFoundException) {
			return NotFoundResponse("The class you're looking for couldn't be found!");
		} else {
			return InternalServerErrorResponse(exception.message);
		}
	}
};

module.exports = classes;