import mongoose from "mongoose";

const Schema = mongoose.Schema;

/**
 * Defining class member schema
 */
const classMember = new mongoose.Schema({
    class: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "Classes"
    },
    student_id: {
        type: Schema.Types.ObjectId,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    },
    deleted_at: Date
});

/**
 * Creating class member model
 * @type {Model}
 */
const ClassMember = mongoose.model("ClassMember", classMember);

/**
 * Exporting class member model
 * @type {Model}
 */
module.exports = ClassMember;