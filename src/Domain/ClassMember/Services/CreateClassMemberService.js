import ClassEntity from "./../../Classes/Entities/ClassEntity";
import ClassMemberEntity from "./../Entities/ClassMemberEntity";
import ClassNotFoundException from "./../../../Application/Exceptions/ClassNotFoundException";

export class CreateClassMemberService {

	constructor (class_code, student_id) {
		this._class_code = class_code;
		this._student_id = student_id;
	}

	async persist () {
		const class = await this.checkClass();
		if (class === null) {
			throw new ClassNotFoundException("Invalid class code!");
		} else {
			return ClassMemberEntity.create({
				class_id: class._id,
				student_id: this._student_id
			}).then(created => {
				return created;
			}).catch(error => {
				throw error;
			});
		}
	}

	async checkClass () {
		return ClassEntity.findOne({
			code: this._class_code
		}).then(class => {
			return class;
		}).catch(error => {
			throw error;
		});
	}
}