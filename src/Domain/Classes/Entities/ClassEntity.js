import mongoose from "mongoose";

const Schema = mongoose.Schema;

/**
 * Defining classes schema
 */
const classes = new mongoose.Schema({
    code: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    teacher_id: {
        type: Schema.Types.ObjectId,
        required: true
    },
    description: String,
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    },
    deleted_at: Date
});

/**
 * Creating classes model
 * @type {Model}
 */
const Classes = mongoose.model("Classes", classes);

/**
 * Exporting classes model
 * @type {Model}
 */
module.exports = Classes;