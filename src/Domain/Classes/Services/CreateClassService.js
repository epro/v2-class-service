import ClassEntity from "./../Entities/ClassEntity";

export class CreateClassService {

	constructor (data) {
		this._data = data;
	}

	async persist () {
		this._data.code = await this.generateCode();
		return ClassEntity.create(this._data).then(created => {
			return created;
		}).catch(error => {
			throw error;
		});
	}

	/**
     * Generate class code
     * 
     * @returns String
     */
    async generateCode() {
        let text = "";
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }
}