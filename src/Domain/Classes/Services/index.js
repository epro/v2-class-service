import CreateClassService from "./CreateClassService";
import UpdateClassService from "./UpdateClassService";
import DeleteClassService from "./DeleteClassService";

export default {
	CreateClassService,
	UpdateClassService,
	DeleteClassService
};