import ClassEntity from "./../Entities/ClassEntity";
import ClassNotFoundException from "./../../../Application/Exceptions/ClassNotFoundException";

export class UpdateClassService {

	constructor (id, data) {
		this._id = id;
		this._data = data;
	}

	async persist () {
		return ClassEntity.findByIdAndUpdate(this._id, {
			$set: this._data
		}, {
			new: true
		}).then(updated => {
			if (updated !== null) {
				return updated;
			} else {
				throw new ClassNotFoundException("The class you're looking for couldn't be found!");
			}
		}).catch(error => {
			throw error;
		})
	}
}