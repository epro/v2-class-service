import ClassEntity from "./../Entities/ClassEntity";
import ClassNotFoundException from "./../../../Application/Exceptions/ClassNotFoundException";

export class DeleteClassService {

	constructor (id) {
		this._id = id;
	}

	async persist () {
		return ClassEntity.findByIdAndUpdate(this._id, {
			$set: {
				deleted_at: Date.now()
			}
		}, {
			new: true
		}).then(updated => {
			if (updated !== null) {
				return updated;
			} else {
				throw new ClassNotFoundException("The class you're looking for couldn't be found!");
			}
		}).catch(error => {
			throw error;
		})
	}
}