/**
 * Unauthorized response builder
 * @param res
 * @param message
 * @param error_code
 * @returns {{success: boolean, message: *, error_code: *, data: null}}
 */
export default (res, message, error_code = 4) => {
    res.status(401);
    res.json({
        success: false,
        message: message,
        error_code: null,
        data: null
    });
}