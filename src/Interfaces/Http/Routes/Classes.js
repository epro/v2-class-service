import ClassesController from "@controllers/ClassesController";

const RouterInstance = require('restify-router').Router;
const router = new RouterInstance();

router.get("/", ClassesController.index);
router.get("/:class_id", ClassesController.show);
router.post("/", ClassesController.store);
router.put("/:class_id", ClassesController.update);
router.del("/:class_id", ClassesController.destroy);

module.exports = router;