const Router = require('restify-router').Router;
const router = new Router();

router.get("/", (req, res) => {
	res.json("SERVER UP!");
});

router.add("/class", require("./Classes"));
router.add("/class/member", require("./ClassMember"));

module.exports = router;