import SuccessResponse from "@responses/SuccessResponse";
import { ClassMemberRepository } from "@repositories/ClassMemberRepository";
import InternalServerErrorResponse from "@responses/InternalServerErrorResponse";

const RouterInstance = require('restify-router').Router;
const router = new RouterInstance();

router.get("/:class_id", async (req, res) => {
	try {
		const result = await new ClassMemberRepository().all(req.params.class_id);
		return SuccessResponse(res, "Class member!", result);
	} catch (exception) {
		return InternalServerErrorResponse(res, exception.message);
	}
});

module.exports = router;