import rp from "request-promise";
import ClassEntity from "./../../Domain/Classes/Entities/ClassEntity";
import { InvalidStateException } from "@exceptions/InvalidStateException";
import { ClassNotFoundException } from "./../../Application/Exceptions/ClassNotFoundException";

export class ClassRepository {

	list (page = 1, per_page = 10) {
		this._page = page;
		this._state = "LIST";
		this._per_page = per_page;
		return this;
	}

	async admin () {
		this._classes = await ClassEntity.find({
			deleted_at: null
		}).select("-__v -created_at -updated_at -deleted_at -description").skip(this._per_page * (this._page - 1)).limit(this._per_page).lean();
		await this.fillTeachersData();
		return this;
	}

	async detailById (id) {
		this._state = "DETAIL";
		this._class = await ClassEntity.findById(id).lean();

		if (this._class !== null) {
			await this.fillTeacherData();
			return this;
		} else {
			throw new ClassNotFoundException("The class you're looking for couldn't be found!");
		}
	}

	async findByCode (code) {
		return ClassEntity.findOne({
			code: code
		}).lean().then(result => {
			if (result !== null) {
				return result;
			} else {
				throw new ClassNotFoundException("The class you're looking for couldn't be found!");
			}
		}).catch(error => {
			throw error;
		});
	}

	async fillTeacherData () {
		const teacher = await rp(`${process.env.AUTH_SERVICE_URI}/teacher/${this._class.teacher_id}`, { json: true });
		delete this._class.teacher_id;
		this._class.teacher = {
			_id: teacher.data._id,
			name: teacher.data.name
		};
	}

	async fillTeachersData () {
		const teachers = await rp(`${process.env.AUTH_SERVICE_URI}/teacher?fetch=all`, { json: true });
        const promises = [];
        this._classes.forEach(cls => {
        	promises.push(
        		teachers.data.forEach(teacher => {
        			if (`${teacher._id}` === `${cls.teacher_id}`) cls.teacher_name = teacher.name;
        		})
        	);
        });
    
        return Promise.all(promises).then(() => {
        	return this;
        });
	}

	get () {
		switch (this._state) {
			case "LIST":
				return this._classes;
			case "DETAIL":
				return this._class;
			default:
				throw new InvalidStateException("Invalid state!");
		}
	}
}