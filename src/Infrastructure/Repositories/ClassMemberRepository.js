import ClassMemberEntity from "./../../Domain/ClassMember/Entities/ClassMemberEntity";

export class ClassMemberRepository {

	async all (class_id, page = 1, per_page = 10) {
		return ClassMemberEntity.find({
			class_id: class_id
		}).lean().then(members => {
			return members;
		}).catch(error => {
			throw error;
		});
	}
}