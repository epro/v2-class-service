import restify from "restify";
import DBConnection from "./config/database";

const server = restify.createServer({
	"name": "class-service",
	ignoreTrailingSlash: true
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
server.pre(restify.plugins.pre.sanitizePath());

const router = require("./src/Interfaces/Http/Routes/index");
router.applyRoutes(server);

DBConnection();

server.listen(process.env.PORT || 4000, () => {
	console.log(`Class service running on port ${process.env.PORT || 4000}`);
});